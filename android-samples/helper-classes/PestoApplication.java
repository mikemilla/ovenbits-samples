package co.thehuman.pesto.controllers.base;

import android.app.Application;

import co.thehuman.pesto.R;
import co.thehuman.pesto.resources.utils.FontOverride;

/**
 * Created by Mike Miller on 5/31/17.
 * This is the class that handles all high level application functions
 */

public class PestoApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Override Fonts
        String[] faces = { "DEFAULT", "MONOSPACE", "SERIF", "SANS_SERIF" };
        FontOverride.overrideFonts(this, faces, R.string.fontCircularBook);
    }
}
