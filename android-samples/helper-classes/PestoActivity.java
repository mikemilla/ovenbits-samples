package co.thehuman.pesto.controllers.base;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Window;

import com.ncapdevi.fragnav.FragNavController;

import java.util.List;

import co.thehuman.pesto.R;
import co.thehuman.pesto.data.models.Company;
import co.thehuman.pesto.resources.utils.Contrast;
import co.thehuman.pesto.resources.views.tabbar.PestoTabBarView;
import co.thehuman.pesto.resources.views.view.PestoDialogView;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by Mike Miller on 5/30/17.
 * This is the base class for all activities
 */

@SuppressWarnings("RestrictedApi")
@SuppressLint("Registered")
public class PestoActivity extends AppCompatActivity implements PestoTabBarView.OnTabBarItemSelectedListener {

    // Variables
    private int mInitialTabIndex = 0;
    private Bundle mSavedInstanceState;
    private PestoTabBarView mTabBar;
    private FragNavController mNavigationController;
    private Menu mTabBarMenu;
    private OnDataReceivedListener mOnDataReceivedListener;
    private Company mCompany;
    private View mContentView;
    private PestoDialogView mDialogView;
    private OnPermissionChangeListener mPermissionChangeListener;

    // Constants
    public static final int USER_LOCATION = 0;
    public static final int READ_CONTACTS = 1;
    public static final int EXTERNAL_STORAGE = 2;
    public static final int CAMERA = 3;

    // Listeners
    public interface OnDataReceivedListener {
        void onDataPassedBack(Intent data);
    }

    /**
     * Sets the activity result data listener
     *
     * @param onDataReceivedListener
     */
    public void setOnDataReceivedListener(PestoActivity.OnDataReceivedListener onDataReceivedListener) {
        mOnDataReceivedListener = onDataReceivedListener;
    }

    /**
     * Callbacks for permission changes
     */
    public interface OnPermissionChangeListener {
        void onLocationPermissionChange(boolean allowedAccess);
    }

    /**
     * Sets the permission change listener
     *
     * @param onPermissionChangeListener
     */
    public void setPermissionChangeListener(OnPermissionChangeListener onPermissionChangeListener) {
        mPermissionChangeListener = onPermissionChangeListener;
    }

    /**
     * OnCreate
     *
     * @param savedInstanceState
     * @param persistentState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        mSavedInstanceState = savedInstanceState;
    }

    /**
     * Sets up the navigation controller
     *
     * @param tabBar
     * @param fragments
     * @param tabBarIndex
     */
    public void setNavigationController(PestoTabBarView tabBar, List<?> fragments, Company company, int tabBarIndex) {

        // Set the theme
        mCompany = company;
        mCompany.setIsLightTheme(Contrast.isColorLight(null, mCompany.getColorPrimary()));
        setStatusBarColor(mCompany);

        // Tab bar reference
        mTabBar = tabBar;
        mTabBar.createTabsWithFragments((List<PestoFragment>) fragments, mCompany.getColorPrimary());
        mTabBar.setTabBarItemSelectedListener(this);

        // Setup fragment navigation controller
        FragNavController.Builder builder = FragNavController.newBuilder(
                mSavedInstanceState,
                getSupportFragmentManager(),
                R.id.fragment_controller).rootFragments((List<Fragment>) fragments);
        mNavigationController = builder.build();

        // Save the content view
        mContentView = findViewById(R.id.app_view);

        // Set the initial launch tab
        mInitialTabIndex = tabBarIndex;
        mNavigationController.switchTab(tabBarIndex);
        mTabBar.setSelectedTabIndex(tabBarIndex);
    }

    /**
     * Gets the view that the content is contained in
     *
     * @return
     */
    public View getContentView() {
        return mContentView;
    }

    /**
     * Returns the company that owns the app
     *
     * @return
     */
    public Company getCompany() {
        return mCompany;
    }

    /**
     * Saves the instance state for navigation controller
     *
     * @param outState
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavigationController != null) {
            mNavigationController.onSaveInstanceState(outState);
        }
    }

    /**
     * Sets the dialog view
     *
     * @param dialogView
     */
    public void setDialogView(PestoDialogView dialogView) {
        mDialogView = dialogView;
    }

    /**
     * Sets the status bar color with a passed string
     *
     * @param company
     */
    public void setStatusBarColor(Company company) {

        // Check for versions of android we can change this on
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // Get window
            Window window = getWindow();

            // Set status bar color
            window.setStatusBarColor(Color.parseColor(company.getColorPrimary()));

            // Set the status bar tint
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                View decor = window.getDecorView();
                if (company.getIsLightTheme()) {
                    decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                } else {
                    decor.setSystemUiVisibility(0);
                }
            }
        }
    }

    /**
     * Sets the status bar color to the default
     */
    public void setStatusBarColorToBlack() {

        // Check for versions of android we can change this on
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            // Get window
            Window window = getWindow();

            // Set status bar color
            window.setStatusBarColor(Color.parseColor("#000000"));

            // Set the status bar tint
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                View decor = window.getDecorView();
                decor.setSystemUiVisibility(0);
            }
        }
    }

    /**
     * Tab Bar selection
     * This is critical to linking the tab bar and the navigation controller
     *
     * @param index
     */
    @Override
    public void onTabBarItemSelected(int index) {
        mNavigationController.switchTab(index);
    }

    /**
     * Tab bar reselection
     *
     * @param index
     */
    @Override
    public void onTabBarItemReselected(int index) {

        // Check if we can drop a fragment
        // Else refresh the current fragment
        if (mNavigationController.getCurrentStack().size() > 1) {
            mNavigationController.clearStack();
        } else {
            getCurrentFragment().onRefresh();
        }
    }

    /**
     * Gets the navigation controller
     *
     * @return
     */
    public FragNavController getNavigationController() {
        return mNavigationController;
    }

    /**
     * Returns the current Pesto Fragment
     *
     * @return
     */
    public PestoFragment getCurrentFragment() {
        return (PestoFragment) mNavigationController.getCurrentFrag();
    }

    /**
     * Sets the menu for the tab bar
     *
     * @param menuResId
     */
    public void setTabBarMenu(int menuResId) {
        mTabBarMenu = new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(menuResId, mTabBarMenu);
    }

    /**
     * Returns the tab bar menu
     *
     * @return
     */
    public Menu getTabBarMenu() {
        return mTabBarMenu;
    }

    /**
     * Handle actual back press clicks
     */
    @Override
    public void onBackPressed() {

        // Check for dialog view
        if (mDialogView != null) {
            mDialogView.hide();
            return;
        }

        // Check of nav controller
        if (mNavigationController != null) {

            // Check for back stack
            if (mNavigationController.getCurrentStack().size() > 1) {
                mNavigationController.popFragment();
            } else {

                // Check for initial tab index
                // Else quit the app
                if (mInitialTabIndex != mTabBar.getSelectedTabIndex()) {
                    mTabBar.setSelectedTabIndex(mInitialTabIndex);
                } else {
                    super.onBackPressed();
                }
            }
        }
    }

    /**
     * Handle activity returning values on load
     *
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);

        // Check for listener
        if (mOnDataReceivedListener != null) {
            mOnDataReceivedListener.onDataPassedBack(data);
        }
    }

    /**
     * Checks a given permission
     *
     * @param permission
     * @return
     */
    public boolean isPermissionGrantedTo(int permission) {

        // Status of permission
        boolean status = false;

        // Check Permission and set status
        switch (permission) {
            case USER_LOCATION: {
                status = checkPermission(ACCESS_FINE_LOCATION)
                        && checkPermission(ACCESS_COARSE_LOCATION);
                break;
            }
            case READ_CONTACTS: {
                status = checkPermission(Manifest.permission.READ_CONTACTS);
                break;
            }
            case EXTERNAL_STORAGE: {
                status = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
                break;
            }
            case CAMERA: {
                status = checkPermission(Manifest.permission.CAMERA)
                        && checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                break;
            }
        }

        // Send the status
        return status;
    }

    /**
     * Checks for access to a given permission
     *
     * @param permission
     * @return
     */
    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Requests a given permission
     *
     * @param permission
     */
    public void requestPermissionAccessTo(int permission) {

        // Check Permission and set status
        switch (permission) {
            case USER_LOCATION: {
                if (!isPermissionGrantedTo(USER_LOCATION)) {
                    ActivityCompat.requestPermissions(
                            this,
                            new String[]{ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION}, USER_LOCATION);
                }
                break;
            }
            case READ_CONTACTS: {
                if (!isPermissionGrantedTo(READ_CONTACTS)) {
                    ActivityCompat.requestPermissions(
                            this,
                            new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACTS);
                }
                break;
            }
            case EXTERNAL_STORAGE: {
                if (!isPermissionGrantedTo(EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(
                            this,
                            new String[]{READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE);
                }
                break;
            }
            case CAMERA: {
                if (!isPermissionGrantedTo(CAMERA))
                    ActivityCompat.requestPermissions(
                            this,
                            new String[]{WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            CAMERA);
                break;
            }
        }
    }

    /**
     * Handles when permission access is changed
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Value of the permission that was changed
        boolean isPermissionGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

        // Supported permissions
        switch (requestCode) {
            case USER_LOCATION:
                mPermissionChangeListener.onLocationPermissionChange(isPermissionGranted);
                break;
            default:
                Log.d("onRequestPermission", "Permission not supported yet");
                break;
        }
    }

}
