package co.thehuman.pesto.controllers.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.ncapdevi.fragnav.FragNavController;

import co.thehuman.pesto.data.models.Company;
import co.thehuman.pesto.resources.utils.Keyboard;
import co.thehuman.pesto.resources.utils.Print;
import co.thehuman.pesto.resources.utils.UserLocationController;

/**
 * Created by Mike Miller on 5/30/17.
 * This is the base class for all fragments in the app
 */

@SuppressLint("ValidFragment")
public class PestoFragment extends Fragment {

    // References
    private PestoActivity mActivity;
    private View mContentView;
    private View mLoadingView;
    private View mErrorView;
    private View mEmptyView;
    private LayoutInflater mInflater;
    private ViewGroup mContainer;
    private Bundle mBundle;
    private int mTitleResId;
    private int mIconResId;
    private MapView mMapView;
    private UserLocationController mUserLocationController;

    public PestoFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Reference to Parent Activity
        if (getActivity() instanceof PestoActivity) {
            mActivity = (PestoActivity) getActivity();
        }
    }

    /**
     * Passed data received from the listener
     *
     * @param onDataReceivedListener
     */
    public void setOnDataReceivedListener(PestoActivity.OnDataReceivedListener onDataReceivedListener) {
        mActivity.setOnDataReceivedListener(onDataReceivedListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        // Check for null view
        if (mContentView == null) {

            // Init Inflaters
            mInflater = inflater;
            mContainer = container;
            mBundle = savedInstanceState;

            // Call view load
            onViewLoad();
        }

        // Call view shown
        onViewShown();

        // Return the view that was created
        return mContentView;
    }

    /**
     * Called when the view loads from {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} if
     * content view has not been inflated
     */
    public void onViewLoad() {
        //
    }

    /**
     * Called when the view is shown to the user
     * Happens every time the view appears
     */
    public void onViewShown() {

        // Calls the other methods when called
        // This is so run time styles in the below super methods happen
        if (mLoadingView != null && mLoadingView.getVisibility() == View.VISIBLE) {
            showLoadingView();
        } else if (mErrorView != null && mErrorView.getVisibility() == View.VISIBLE) {
            showErrorView();
        } else if (mEmptyView != null && mEmptyView.getVisibility() == View.VISIBLE) {
            showEmptyView();
        } else {
            showContentView();
        }
    }

    /**
     * Called when the view is removed from sight
     */
    public void onViewRemoved() {

    }

    /**
     * Sets the title and icon id
     *
     * @param titleResId * @param iconResId
     */
    public PestoFragment withTab(int titleResId, int iconResId) {
        mTitleResId = titleResId;
        mIconResId = iconResId;
        return this;
    }

    /**
     * Sets the map view for the fragment
     *
     * @param resId
     */
    public void setMapView(int resId, OnMapReadyCallback onMapReadyCallbacks) {

        // Set map and callbacks
        mMapView = (MapView) findViewById(resId);

        // Show map
        mMapView.onCreate(mBundle);
        mMapView.onResume();

        // Handle error
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set callbacks
        mMapView.getMapAsync(onMapReadyCallbacks);
    }

    /**
     * Returns the reference to the map view
     * @return
     */
    public MapView getMapView() {
        return mMapView;
    }

    /**
     * Returns the current company
     * This is a shortcut
     * @return
     */
    public Company getCompany() {
        return mActivity.getCompany();
    }

    /**
     * Sets the views
     *
     * @param layout
     */
    public void setContentView(int layout) {
        mContentView = mInflater.inflate(layout, mContainer, false);
    }

    public void setLoadingView(int layout) {
        mLoadingView = addViewState(mLoadingView, layout);
    }

    public void setErrorView(int layout) {
        mErrorView = addViewState(mErrorView, layout);
    }

    public void setEmptyView(int layout) {
        mEmptyView = addViewState(mEmptyView, layout);
    }

    /**
     * Adds the view to the content view
     */
    private View addViewState(View viewState, int layout) {

        // Remove if added
        if (viewState != null) {
            ((ViewGroup) mContentView).removeView(viewState);
        }

        // Inflate view
        viewState = mInflater.inflate(layout, ((ViewGroup) mContentView), false);
        viewState.setVisibility(View.GONE);
        ((ViewGroup) mContentView).addView(viewState);

        // Pass ref
        return viewState;
    }

    /**
     * Returns the views
     *
     * @return
     */
    public ViewGroup getContentView() {
        return (ViewGroup) mContentView;
    }

    public View getLoadingView() {
        return mLoadingView;
    }

    public View getErrorView() {
        return mErrorView;
    }

    public View getEmptyView() {
        return mEmptyView;
    }

    /**
     * Shows the correct view for the state
     */
    public void showContentView() {
        setVisibilityIfAble(mLoadingView, View.GONE);
        setVisibilityIfAble(mErrorView, View.GONE);
        setVisibilityIfAble(mEmptyView, View.GONE);
    }

    public void showLoadingView() {
        setVisibilityIfAble(mLoadingView, View.VISIBLE);
        setVisibilityIfAble(mErrorView, View.GONE);
        setVisibilityIfAble(mEmptyView, View.GONE);
    }

    public void showErrorView() {
        setVisibilityIfAble(mLoadingView, View.GONE);
        setVisibilityIfAble(mErrorView, View.VISIBLE);
        setVisibilityIfAble(mEmptyView, View.GONE);
    }

    public void showEmptyView() {
        setVisibilityIfAble(mLoadingView, View.GONE);
        setVisibilityIfAble(mErrorView, View.GONE);
        setVisibilityIfAble(mEmptyView, View.VISIBLE);
    }

    /**
     * Sets the visibility for the view is possible
     *
     * @param view
     * @param visibility
     */
    private void setVisibilityIfAble(View view, int visibility) {
        if (view != null) {
            view.setVisibility(visibility);
        }
    }

    /**
     * Gets the icon resource id
     *
     * @return
     */
    public int getIconResId() {
        return mIconResId;
    }

    /**
     * Gets the title resource id
     *
     * @return
     */
    public int getTitleResId() {
        return mTitleResId;
    }

    /**
     * Allows you to find a view by id
     * Used like an activity uses findViewById
     *
     * @param parentView
     * @param id
     * @return
     */
    public View findViewById(View parentView, int id) {
        return parentView.findViewById(id);
    }

    public View findViewById(int id) {
        return mContentView.findViewById(id);
    }

    /**
     * Allows you to push a fragment to the nav controller
     * Uses the instance of the activity to perform
     *
     * @param fragment
     */
    public void push(Fragment fragment) {
        getNavigationController().pushFragment(fragment);
    }

    /**
     * Removes the fragment from the stack
     */
    public void pop() {

        // Remove fragment
        getNavigationController().popFragment();

        // Close Keyboard if possible
        Keyboard.hide(mActivity);
    }

    /**
     * Sets a location listener
     * Allows you to use the fragment to detect changes to user locations
     * @param onUserLocationChangeListener
     */
    public void setUserLocationChangeListener(UserLocationController.OnUserLocationChangeListener onUserLocationChangeListener) {
        mUserLocationController = new UserLocationController(onUserLocationChangeListener);
    }

    /**
     * Gets the user location controller
     * @return
     */
    public UserLocationController getUserLocationController() {
        return mUserLocationController;
    }

    /**
     * Shortcut to check a given permission
     *
     * @param permission
     * @return
     */
    public boolean isPermissionGrantedTo(int permission) {
        return mActivity.isPermissionGrantedTo(permission);
    }

    /**
     * Shortcut to requesting permission from parent
     *
     * @param permission
     */
    public void requestPermissionAccessTo(int permission) {
        mActivity.requestPermissionAccessTo(permission);
    }

    /**
     * Sets the permission change listener
     *
     * @param onPermissionChangeListener
     */
    public void setPermissionChangeListener(PestoActivity.OnPermissionChangeListener onPermissionChangeListener) {
        mActivity.setPermissionChangeListener(onPermissionChangeListener);
    }

    /**
     * Returns the bundle
     *
     * @return
     */
    public Bundle getBundle() {
        return mBundle;
    }

    /**
     * Called when the tab is reselected
     */
    public void onRefresh() {
        // Empty
    }

    /**
     * Get the activity that contains the fragment
     *
     * @return
     */
    public PestoActivity getParentActivity() {
        return mActivity;
    }

    /**
     * Returns current navigation controller
     *
     * @return
     */
    public FragNavController getNavigationController() {
        return mActivity.getNavigationController();
    }

    /**
     * Handle Map and fragment states
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        // Call view removed
        onViewRemoved();

        // Handle map
        if (mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mMapView != null) {
            mMapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mMapView != null) {
            mMapView.onLowMemory();
        }
    }
}
