package co.thehuman.pesto.data.apis;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import co.thehuman.pesto.data.models.Company;
import co.thehuman.pesto.data.models.Concept;
import co.thehuman.pesto.data.models.PestoResponse;
import co.thehuman.pesto.resources.utils.Print;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by michaelmiller on 7/18/17.
 * This is the top level class that all api calls run through.
 */

// Example api usage
// PestoAPI mAPI = new PestoAPI();
// mAPI.getCompany(companyId, new PestoAPI.PestoCallbacks() {
//     @Override
//     public void onSuccess(Company company) {
//         showContentView(company);
//     }
//
//     @Override
//     public void onFailure(Throwable throwable) {
//         showErrorView(throwable.getMessage());
//     }
// });

public class PestoAPI {

    // Variables
    public boolean isCalling = false;
    public boolean didCancelCall = true;
    private PestoCalls calls;
    private static final String URL_STAGING = "http://pesto-staging.thehuman.co/api/v1/";
    private static final String URL_PRODUCTION = "http://pesto-production.thehuman.co/api/v1/";

    // Constructors
    public PestoAPI() {

        // Build Call
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_STAGING)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create calls
        calls = retrofit.create(PestoCalls.class);
    }

    // Available API Calls
    interface PestoCalls {
        @GET("concept")
        Call<PestoResponse> getCompany(@Query("id") Integer id);
    }

    // Callbacks
    public interface PestoCallbacks {
        void onSuccess(Company company);

        void onFailure(Throwable throwable);
    }

    // API Methods
    public void getCompany(Integer id, @NonNull final PestoCallbacks callbacks) {

        // Call is in transit
        isCalling = true;

        // Perform the call
        // We do this here because retrofit has some bloat that we don't really care about
        calls.getCompany(id).enqueue(new Callback<PestoResponse>() {
            @Override
            public void onResponse(@NonNull Call<PestoResponse> call, @NonNull Response<PestoResponse> response) {

                // Current statue variables
                isCalling = false;
                didCancelCall = true;

                // Map the response
                try {
                    Concept concept = new Gson().fromJson(response.body().getData(), Concept.class);
                    callbacks.onSuccess(concept.getConcept());
                } catch (Throwable throwable) {
                    callbacks.onFailure(throwable);
                }
            }

            @Override
            public void onFailure(@NonNull Call<PestoResponse> call, @NonNull Throwable throwable) {
                if (!didCancelCall) {
                    isCalling = false;
                    callbacks.onFailure(throwable);
                }
            }
        });
    }
}
