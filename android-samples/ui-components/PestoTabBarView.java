package co.thehuman.pesto.resources.views.tabbar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import co.thehuman.pesto.R;
import co.thehuman.pesto.controllers.base.PestoFragment;
import co.thehuman.pesto.resources.utils.Size;
import co.thehuman.pesto.resources.views.view.PestoFlatShadow;

/**
 * Created by Mike Miller on 5/29/17.
 * This is the class that is the Tab Bar at the bottom of the screen
 */

public class PestoTabBarView extends LinearLayout {

    // Listener
    public interface OnTabBarItemSelectedListener {
        void onTabBarItemSelected(int index);
        void onTabBarItemReselected(int index);
    }

    // Variables
    private int mSelectedTabIndex = 0; // Default selected tab index
    private LinearLayout mTabContainer;
    private List<PestoTabBarItem> mTabs;
    private OnTabBarItemSelectedListener mTabBarItemSelectedListener;

    // Constructors
    public PestoTabBarView(Context context) {
        this(context, null);
    }

    public PestoTabBarView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    // Gets the menu from resource & builds the tab bar
    @SuppressLint("RestrictedApi")
    public PestoTabBarView(final Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        // Check if tab container exists
        if (mTabContainer == null) {

            // Set orientation
            setOrientation(LinearLayout.VERTICAL);

            // Add shadow
            addView(new PestoFlatShadow(context));

            // Add tab bar container
            mTabContainer = new LinearLayout(context);
            mTabContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBackground));
            mTabContainer.setOrientation(LinearLayout.HORIZONTAL);
            mTabContainer.setLayoutParams(new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    Size.getDimensionFromRes(R.dimen.default_button, context)));
            addView(mTabContainer);

            // Init tab array
            // This is used to help us conveniently reference the tabs
            mTabs = new ArrayList<>();
        }
    }

    /**
     * Creates the tabs with fragments
     * @param fragments
     */
    public void createTabsWithFragments(List<PestoFragment> fragments, String tintColor) {

        // Add tab items
        for (int i = 0; i < fragments.size(); i ++) {

            // Get item at index
            final PestoFragment fragmentAtIndex = fragments.get(i);

            // Setup item
            final PestoTabBarItem pestoTabBarItem = new PestoTabBarItem(getContext());
            pestoTabBarItem.setTitle(fragmentAtIndex.getTitleResId());
            pestoTabBarItem.setIcon(fragmentAtIndex.getIconResId());
            pestoTabBarItem.setTintColor(tintColor);

            // Add interaction to tab
            addActionsToTabAtIndex(pestoTabBarItem, i);

            // Add item to view
            mTabContainer.addView(pestoTabBarItem);

            // Add item to tab list
            mTabs.add(pestoTabBarItem);
        }
    }

    /**
     * Adds interaction to a given tab
     * Condenses loops
     * @param pestoTabBarItem
     * @param index
     */
    private void addActionsToTabAtIndex(PestoTabBarItem pestoTabBarItem, final int index) {

        // Normal click action
        pestoTabBarItem.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                // Check for tab reselection
                if (index == mSelectedTabIndex && mTabBarItemSelectedListener != null) {
                    mTabBarItemSelectedListener.onTabBarItemReselected(index);
                    return;
                }

                // Set new selected index
                setSelectedTabIndex(index);
            }
        });

        // Long press action
        pestoTabBarItem.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                // Check for tab reselection
                if (index == mSelectedTabIndex && mTabBarItemSelectedListener != null) {
                    mTabBarItemSelectedListener.onTabBarItemReselected(index);
                    return true;
                }

                // Set new selected index
                setSelectedTabIndex(index);

                // Fallback
                return true;
            }
        });
    }

    /**
     * Sets the tab bar selection listener
     * @param tabBarItemSelectedListener
     */
    public void setTabBarItemSelectedListener(OnTabBarItemSelectedListener tabBarItemSelectedListener) {
        mTabBarItemSelectedListener = tabBarItemSelectedListener;
    }

    /**
     * Gets the current tab index
     * @return
     */
    public int getSelectedTabIndex() {
        return mSelectedTabIndex;
    }

    /**
     * Sets the current tab index
     * @param selectedTabIndex
     */
    public void setSelectedTabIndex(int selectedTabIndex) {

        // Set current selected tab
        mSelectedTabIndex = selectedTabIndex;

        // Loop through tabs and style the selection
        for (int i = 0; i < mTabs.size(); i++) {
            if (i == mSelectedTabIndex) {
                mTabs.get(i).setSelected(true);
            } else {
                mTabs.get(i).setSelected(false);
            }
        }

        // Check for tab bar selection listener
        if (mTabBarItemSelectedListener != null) {
            mTabBarItemSelectedListener.onTabBarItemSelected(selectedTabIndex);
        }
    }
}
