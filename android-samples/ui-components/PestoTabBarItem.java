package co.thehuman.pesto.resources.views.tabbar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import co.thehuman.pesto.R;
import co.thehuman.pesto.resources.utils.Contrast;
import co.thehuman.pesto.resources.views.imageview.PestoIconView;
import co.thehuman.pesto.resources.views.textview.PestoTextView;

/**
 * Created by Mike Miller on 5/29/17.
 * This is the Tab Bar item
 */

public class PestoTabBarItem extends FrameLayout {

    // Variables
    private View tabItemLayout;
    private PestoIconView mIconView;
    private PestoTextView mTextView;
    private MenuItem mMenuItem;
    private String mTintColor;

    // Constructors
    public PestoTabBarItem(@NonNull Context context) {
        super(context);
        init(context);
    }

    public PestoTabBarItem(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PestoTabBarItem(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    // Init the view
    private void init(Context context) {

        // Disable haptic vibration
        setHapticFeedbackEnabled(false);

        // Inflate tab item view from xml
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        tabItemLayout = inflater.inflate(R.layout.pesto_tabbar_item, null);
        addView(tabItemLayout);

        // Set layout params to container
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT);
        params.weight = 1;
        setLayoutParams(params);
    }

    /**
     * Set menu item
     * This allows us to reference things easier
     * @param menuItem
     */
    public void setMenuItem(MenuItem menuItem) {
        setTitle(menuItem.getTitle());
        setIcon(menuItem.getIcon());
        mMenuItem = menuItem;
    }

    /**
     * Returns the menu item
     * @return
     */
    public MenuItem getMenuItem() {
        return mMenuItem;
    }

    /**
     * Sets the title of the button
     * @param title
     */
    public void setTitle(CharSequence title) {
        mTextView = (PestoTextView) tabItemLayout.findViewById(R.id.text_view);
        mTextView.setText(title.toString());
    }

    public void setTitle(int resId) {
        mTextView = (PestoTextView) tabItemLayout.findViewById(R.id.text_view);
        mTextView.setText(getResources().getString(resId));
    }

    /**
     * Sets the icon on the button
     * @param drawable
     */
    public void setIcon(Drawable drawable) {
        mIconView = (PestoIconView) tabItemLayout.findViewById(R.id.image_view);
        mIconView.setImageDrawable(drawable);
    }

    public void setIcon(int resId) {
        mIconView = (PestoIconView) tabItemLayout.findViewById(R.id.image_view);
        mIconView.setImageDrawable(ContextCompat.getDrawable(getContext(), resId));
    }

    /**
     * Sets the tint color for the icon and title
     * @param tintColor
     */
    public void setTintColor(String tintColor) {

        // Check if color is too light
        if (Contrast.isColorLight(Contrast.CONTRAST_THRESHOLD, tintColor)) {
            mTintColor = "#" + Integer.toHexString(ContextCompat.getColor(getContext(), R.color.colorDarkGrey));
        } else {
            mTintColor = tintColor;
        }
    }

    /**
     * Styles the button for the selected state
     * @param selected
     */
    public void setSelected(boolean selected) {
        if (selected) {
            mIconView.setTint(mTintColor);
            mTextView.setTint(mTintColor);
        } else {
            mIconView.setTint(R.color.colorLightGrey);
            mTextView.setTint(R.color.colorLightGrey);
        }
    }
}
