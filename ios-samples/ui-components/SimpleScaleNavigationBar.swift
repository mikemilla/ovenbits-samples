//
//  SimpleScaleNavigationBar.swift
//  Simple Scale
//
//  Created by Michael Miller on 10/17/16.
//  Copyright © 2016 Mike Milla. All rights reserved.
//

import UIKit

@IBDesignable class SimpleScaleNavigationBar: UINavigationBar {
    
    // MARK: Inspectables
    @IBInspectable var transparent:Bool = false {
        didSet {
            if (transparent) {
                showTransparentView()
            } else {
                showSolidView()
            }
        }
    }
    
    // MARK: Constants
    private let contentOffset = "contentOffset"
    static let defaultHeight:CGFloat = 64
    
    // MARK: Variables
    private var statusBarView:UIView = UIView()
    private var offset:CGFloat = 0
    private var scrollView:UIScrollView? {
        didSet {
            
            // Check for nil
            if let scrollView = self.scrollView {
                scrollView.addObserver(self, forKeyPath: contentOffset, options: .new, context: nil)
            }
        }
    }
    
    var title:String? {
        didSet {
            topItem?.title = title
        }
    }

    // MARK: UINavigationBar Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initialize()
    }
    
    // MARK: Build
    func initialize() {
        
        // Set Translucency
        isTranslucent = true
        
        // Set Styles
        tintColor = Colors.White
        barTintColor = UIColor.clear
        
        // Set Font
        let fontButtons = UIFont(name: Fonts.GothamRoundedMedium, size: 16.0)
        let fontTitle = UIFont(name: Fonts.GothamRoundedMedium, size: 18.0)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: fontButtons!, NSForegroundColorAttributeName: Colors.White], for: .normal)
        titleTextAttributes = [NSFontAttributeName: fontTitle!, NSForegroundColorAttributeName: Colors.White]
        
        // Remove Hairline
        setBackgroundImage(UIImage(), for: .default)
        shadowImage = UIImage()
    }
    
    // MARK: View Types
    private func showSolidView() {
        
        // Set Shadow
        showShadow()
        
        // Set Bar Styles
        alpha = 1
        isTranslucent = false
        barTintColor = Colors.Primary
    }
    
    private func showTransparentView() {
        
        // Set Shadow
        hideShadow()
        
        // Set Bar Styles
        alpha = 1
        isTranslucent = true
        barTintColor = UIColor.clear
    }
    
    // MARK: Shadow States
    private func showShadow() {
        layer.shadowRadius = 4
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.26
    }
    
    private func hideShadow() {
        layer.shadowRadius = 1
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 0
    }
}
