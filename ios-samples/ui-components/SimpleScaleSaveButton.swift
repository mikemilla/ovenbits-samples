//
//  SimpleScaleSaveButton.swift
//  Simple Scale
//
//  Created by Michael Miller on 11/3/16.
//  Copyright © 2016 Mike Milla. All rights reserved.
//

import UIKit

class SimpleScaleSaveButton: UIButton {
    
    // MARK: Variables
    var didLayoutSubviews:Bool = false
    var shown:Bool = true
    var containerView:UIView!
    let gradient:CAGradientLayer = CAGradientLayer()
    let duration:TimeInterval = 0.33
    
    // MARK: UIView Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        style()
    }
    
    // MARK: Style
    func style() {
        
        // Remove all subviewss
        for view in subviews {
            view.removeFromSuperview()
        }
        
        // Set Inital Styles
        backgroundColor = Colors.PrimaryDark
        layer.cornerRadius = bounds.height / 2
        translatesAutoresizingMaskIntoConstraints = false
        
        // Container
        containerView = UIView()
        containerView.layer.cornerRadius = bounds.height / 2
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.isUserInteractionEnabled = false
        containerView.clipsToBounds = true
        addSubview(containerView)
        
        // Container Constraints
        let containerViewLeft = NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let ccontainerViewRight = NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let containerViewTop = NSLayoutConstraint(item: containerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let containerViewBottom = NSLayoutConstraint(item: containerView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        
        // Add check mark image
        let checkImageView = UIImageView(image: Icons.Check)
        checkImageView.translatesAutoresizingMaskIntoConstraints = false
        checkImageView.contentMode = .center
        
        // Setup constraints
        let checkImageViewLeft = NSLayoutConstraint(item: checkImageView, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: 24)
        let checkImageViewTop = NSLayoutConstraint(item: checkImageView, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1, constant: 0)
        let checkImageViewBottom = NSLayoutConstraint(item: checkImageView, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0)
        let checkImageViewWidth = NSLayoutConstraint(item: checkImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 24)
        
        // Add check mark
        containerView.addSubview(checkImageView)

        // Add "Save" label
        let saveLabel = SimpleScaleLabel()
        saveLabel.textAlignment = .center
        saveLabel.text = NSLocalizedString("SAVE", comment: "Save")
        saveLabel.mediumFont = true
        saveLabel.color = SimpleScaleLabel.White
        saveLabel.font = saveLabel.font.withSize(20)
        saveLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Setup constraints
        let saveLabelLeft = NSLayoutConstraint(item: saveLabel, attribute: .leading, relatedBy: .equal, toItem: containerView, attribute: .leading, multiplier: 1, constant: 56)
        let saveLabelRight = NSLayoutConstraint(item: saveLabel, attribute: .trailing, relatedBy: .equal, toItem: containerView, attribute: .trailing, multiplier: 1, constant: -24) // Must be negative
        let saveLabelTop = NSLayoutConstraint(item: saveLabel, attribute: .top, relatedBy: .equal, toItem: containerView, attribute: .top, multiplier: 1, constant: 0)
        let saveLabelBottom = NSLayoutConstraint(item: saveLabel, attribute: .bottom, relatedBy: .equal, toItem: containerView, attribute: .bottom, multiplier: 1, constant: 0)
        let saveLabelWidth = NSLayoutConstraint(item: saveLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: saveLabel.intrinsicContentSize.width)

        // Add container view
        containerView.addSubview(saveLabel)
        
        // Add constraints to the parent view
        NSLayoutConstraint.activate([containerViewLeft, ccontainerViewRight, containerViewTop, containerViewBottom,
                                     checkImageViewWidth, checkImageViewLeft, checkImageViewTop, checkImageViewBottom,
                                     saveLabelLeft, saveLabelRight, saveLabelTop, saveLabelBottom, saveLabelWidth])
        
        // Set Shadow
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowOpacity = 0.26
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowRadius = 6
        
        // Add Gradient
        setNormalColor()
        
        // Add Click Observers
        setButtonObservers(sender: self)
    }
    
    // MARK: Visibility
    func show(animated: Bool) {
        
        // Shown value
        shown = true
        
        // Turn on interaction
        isUserInteractionEnabled = true
        
        // Init value
        alpha = 0
        
        // Hide the view
        if (animated) {
            UIView.animate(
                withDuration: duration,
                delay: 0.0,
                usingSpringWithDamping: 0.7,
                initialSpringVelocity: 0.4,
                animations: {
                    self.alpha = 1
                    self.transform = CGAffineTransform(scaleX: 1, y: 1)
            }, completion: {
                (value: Bool) in
                //
            })
            
        } else {
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
    
    func hide(animated: Bool) {
        
        // Hidden Value
        shown = false
        
        // Turn off interaction
        isUserInteractionEnabled = false
        
        // Init value
        alpha = 1
        
        // Hide the view
        if (animated) {
            UIView.animate(
                withDuration: duration / 2,
                delay: 0.0,
                animations: {
                    self.alpha = 0
                    self.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            }, completion: {
                (value: Bool) in
                //
            })
        } else {
            self.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            self.alpha = 0
        }
    }
    
    // MARK: Background colors
    private func setNormalColor() {
        gradient.frame = bounds
        gradient.colors = [Colors.Primary.cgColor, Colors.PrimaryLight.cgColor]
        containerView.layer.insertSublayer(gradient, at: 0)
    }
    
    private func setPressedColor() {
        gradient.removeFromSuperlayer()
    }
    
    // MARK: Obversers
    private func setButtonObservers(sender: SimpleScaleSaveButton) {
        sender.addTarget(self, action: #selector(SimpleScaleSaveButton.buttonTouchDown(sender:)), for: .touchDown)
        sender.addTarget(self, action: #selector(SimpleScaleSaveButton.buttonTouchUpInside(sender:)), for: .touchDragExit)
        sender.addTarget(self, action: #selector(SimpleScaleSaveButton.buttonTouchUpInside(sender:)), for: .touchUpInside)
    }
    
    // MARK: Click States
    @objc private func buttonTouchDown(sender: SimpleScaleSaveButton) {
        
        // Set background color
        setPressedColor()
        
        // Animate scale down
        UIView.animate(
            withDuration: 0.1,
            delay: 0.0,
            options: .allowUserInteraction,
            animations: {
                self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            }, completion: nil)
    }
    
    @objc private func buttonTouchUpInside(sender: SimpleScaleSaveButton) {
        
        // Set background color
        setNormalColor()
        
        // Animate Scale Up
        UIView.animate(
            withDuration: 0.1,
            delay: 0.0,
            options: .allowUserInteraction,
            animations: {
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
            }, completion: nil)
    }

}
