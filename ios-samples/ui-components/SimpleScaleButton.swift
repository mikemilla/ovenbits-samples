//
//  SimpleScaleButton.swift
//  Simple Scale
//
//  Created by Michael Miller on 11/15/16.
//  Copyright © 2016 Mike Milla. All rights reserved.
//

import UIKit

@IBDesignable class SimpleScaleButton: UIButton {
    
    // MARK: Inspectables
    @IBInspectable public var normalColor:UIColor = UIColor.red {
        didSet {
            initialize()
        }
    }
    @IBInspectable public var pressedColor:UIColor = UIColor.green
    @IBInspectable public var roundedCorners:Bool = false {
        didSet {
            initialize()
        }
    }
    
    // Set Icon
    var icon:UIImage! {
        didSet {
            layoutSubviews()
        }
    }
    
    var title:String? {
        didSet {
            setTitle(title, for: .normal)
        }
    }
    
    var color:UIColor? {
        didSet {
            setTitleColor(color, for: .normal)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Check for icon only in ios 10+
        if #available(iOS 10, *) {
            if (icon != nil) {
                
                // Prevent animations
                UIView.setAnimationsEnabled(false)
                
                // Remove all other subviews
                for subview in subviews {
                    subview.removeFromSuperview()
                }
                
                // Remove title label
                setTitle(nil, for: .normal)
                
                // Init Container
                let container = UIView()
                container.isUserInteractionEnabled = false
                
                // Add Icon View
                let iconView = UIImageView()
                iconView.frame = CGRect(x: 0, y: 0, width: 24, height: bounds.height)
                iconView.contentMode = .center
                iconView.image = icon
                container.addSubview(iconView)
                
                // Add Title Label
                let titleView = SimpleScaleLabel()
                titleView.text = titleLabel?.text
                titleView.font = UIFont(name: Fonts.GothamRoundedMedium, size: 16)
                titleView.textColor = Colors.White
                titleView.frame = CGRect(x: iconView.frame.width + 16, y: 0, width: titleView.intrinsicContentSize.width, height: bounds.height)
                container.addSubview(titleView)
                
                // Set position
                container.frame.size.width = iconView.frame.width + 16 + titleView.frame.width
                container.frame.size.height = bounds.height
                container.frame.origin.x = (bounds.width - container.frame.width) / 2
                addSubview(container)
                
                // Allow animations
                UIView.setAnimationsEnabled(true)
            }
        }
    }

    // MARK: Init
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        initialize()
    }
    
    // MARK: Build
    private func initialize() {
        
        // Set Font
        titleLabel!.font = UIFont(name: Fonts.GothamRoundedMedium, size: titleLabel!.font.pointSize)
        
        // Set Color
        backgroundColor = normalColor
        
        // Set Corners
        if (roundedCorners) {
            layer.cornerRadius = 6
        }
        
        // Clip to bounds
        clipsToBounds = true
        
        // Set Click observers
        setButtonObservers(sender: self)
    }
    
    // MARK: Obversers
    private func setButtonObservers(sender: SimpleScaleButton) {
        sender.addTarget(self, action: #selector(SimpleScaleButton.buttonTouchDown(sender:)), for: .touchDown)
        sender.addTarget(self, action: #selector(SimpleScaleButton.buttonTouchUpInside(sender:)), for: .touchDragExit)
        sender.addTarget(self, action: #selector(SimpleScaleButton.buttonTouchUpInside(sender:)), for: .touchUpInside)
    }
    
    // MARK: Click States
    @objc private func buttonTouchDown(sender: SimpleScaleSaveButton) {
        backgroundColor = pressedColor
    }
    
    @objc private func buttonTouchUpInside(sender: SimpleScaleSaveButton) {
        backgroundColor = normalColor
    }

}
