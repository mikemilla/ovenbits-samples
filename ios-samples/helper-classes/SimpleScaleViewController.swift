//
//  SimpleScaleViewController.swift
//  Simple Scale
//
//  Created by Michael Miller on 11/3/16.
//  Copyright © 2016 Mike Milla. All rights reserved.
//

import UIKit

class SimpleScaleViewController: UIViewController {

    // MARK: Variables
    private let transition = SimpleScalePresentationTransition()
    
    // MARK: Present
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        
        // Add transition style
        viewControllerToPresent.modalPresentationStyle = .custom
        viewControllerToPresent.transitioningDelegate = transition
        
        // Call super after attaching style
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    // MARK: View Delegates
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Change Status Bar Color
        UIApplication.shared.statusBarStyle = .lightContent
    }
}

class SimpleScalePresentationTransition: NSObject, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    
    // MARK: Variables
    private var duration:TimeInterval = 0.33
    private var isPresenting:Bool = true
    private var transitionContext:UIViewControllerContextTransitioning!
    
    // MARK: Transition Delegates
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = true
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresenting = false
        return self
    }
    
    // MARK: Transition Caller
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        // Get views and context
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        self.transitionContext = transitionContext
        
        // Check if we are entering or leaving
        if (isPresenting) {
            
            // Fixes bug iOS 9+
            toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
            containerView.addSubview(toViewController.view)
            presentTransition(containerView: containerView, fromViewController: fromViewController, toViewController: toViewController)
        } else {
            dismissTransition(containerView: containerView, fromViewController: fromViewController, toViewController: toViewController)
        }
    }
    
    // MARK: Enter
    func presentTransition(containerView: UIView, fromViewController: UIViewController, toViewController: UIViewController) {
        
        fromViewController.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        fromViewController.view.alpha = 1
        fromViewController.view.frame.origin.y = 0
        toViewController.view.frame.origin.y = UIScreen.main.bounds.height
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            usingSpringWithDamping: 0.8,
            initialSpringVelocity: 0.3,
            options: .allowUserInteraction,
            animations: {
                fromViewController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                fromViewController.view.alpha = 0.5
                fromViewController.view.frame.origin.y = 44
                toViewController.view.frame.origin.y = 0
            }, completion: {
                (value: Bool) in
                self.finish()
        })
    }
    
    // MARK: Exit
    func dismissTransition(containerView: UIView, fromViewController: UIViewController, toViewController: UIViewController) {
        
        toViewController.view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        toViewController.view.alpha = 0.5
        toViewController.view.frame.origin.y = 44
        fromViewController.view.frame.origin.y = 0
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 0.9,
            options: .allowUserInteraction,
            animations: {
                fromViewController.view.frame.origin.y = UIScreen.main.bounds.height + 44
            }, completion: {
                (value: Bool) in
                //
        })
        
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: .curveEaseInOut,
            animations: {
                toViewController.view.transform = CGAffineTransform(scaleX: 1, y: 1)
                toViewController.view.alpha = 1
                toViewController.view.frame.origin.y = 0
            }, completion: {
                (finished: Bool) -> Void in
                self.finish()
        })
    }
    
    // Used to "Clean up" view controllers when transition completes
    private func finish() {
        if (!isPresenting) {
            let fromViewController = transitionContext?.viewController(forKey: UITransitionContextViewControllerKey.from)!
            fromViewController?.view.removeFromSuperview()
        }
        transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
    
}
