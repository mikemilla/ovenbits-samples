//
//  Pin.swift
//  SportsmanTracker
//
//  Created by Mike Miller on 7/22/16.
//  Copyright � 2016 Sportsman Tracker. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import ObjectMapper

public class PinResponse: Object, Mappable {
    
    // Variables
    var user_pin = PinModel?()
    
    // Init
    required convenience public init?(_ map: Map) {
        self.init()
    }
    
    // Mapping
    public func mapping(map: Map) {
        user_pin <- map[ParameterKey.user_pin]
    }
}

public class PinArray: Object, Mappable {
    
    // Variables
    var pins = List<PinModel>()
    
    // Init
    required convenience public init?(_ map: Map) {
        self.init()
    }
    
    // Mapping
    public func mapping(map: Map) {
        pins <- (map[ParameterKey.pins], ArrayTransform<PinModel>())
    }
}

class PinModel: Object, Mappable {
    
    // Non-Optionals
    var user_pin_id:     String!
    var latitude:        Double!
    var longitude:       Double!
    
    // Optionals
    private var slug:            String?
    private var name:            String?
    private var sport_type_id:   String?
    private var sport_type_name: String?
    private var species =        List<SpeciesModel>?()
    
    // Init
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    // Mapping
    func mapping(map: Map) {
        user_pin_id     <- map[ParameterKey.user_pin_id]
        name            <- map[ParameterKey.name]
        slug            <- map[ParameterKey.slug]
        latitude        <- map[ParameterKey.latitude]
        longitude       <- map[ParameterKey.longitude]
        sport_type_id   <- map[ParameterKey.sport_type_id]
        sport_type_name <- map[ParameterKey.sport_type_name]
        species         <- (map[ParameterKey.species], ArrayTransform<SpeciesModel>())
    }
    
}

// MARK: Calling Class
class PinAPI {
    
    // Ref to smtAPI
    let api:SportsmanTrackerAPI = SportsmanTrackerAPI()
    
    // Set Delegate
    var delegate:PinAPIDelegate? = nil
    
    // Perform Request
    func request(request: PinAPIRequest) {
        
        // Handle Missing Delegate
        if (delegate == nil) {
            print("PinAPIDelegate is missing. Add the delegate to handle response properly")
            return
        }
        
        // Hit API
        api.call(
            typeOfRequest: request,
            success: { (value) in
                
                // Call delegate accordingly
                switch request {
                    
                // DELETE
                case .deletePin:
                    self.delegate!.delelePinSuccess()
                    
                // GET
                case .getPin:
                    if let response = Mapper<PinArray>().map(value[self.api.dataParameter]) {
                        self.delegate!.getPinSuccess(response.pins)
                        return
                    }
                    self.delegate!.getPinFailure()
                    
                // POST
                case .postPin(let name, let latitude, let longitude, let locationId, _): // (name: String?, latitude: Double!, longitude: Double!, user_pin_id: String?)
                    
                    if let response = Mapper<PinResponse>().map(value[self.api.dataParameter]) {
                        
                        // MARK: Firebase Event
                        eventTracking.logEvent(
                            TrackingEvents.UserPinCreate,
                            parameters: [
                                "latitude": latitude,
                                "longitude": longitude,
                                "name": name!
                            ])
                        
                        // Success Delegate
                        if let userPin = response.user_pin {
                            self.delegate!.postPinSuccess(userPin, locationId: locationId)
                        }
                        
                        // End
                        return
                    }
                    
                    // Fallback Delegate
                    self.delegate!.postPinFailure()
                }
        },
            
            failure: { (error) in
                
                // Call delegate accordingly
                switch request {
                // DELETE
                case .deletePin:
                    self.delegate!.deletePinFailure()
                    
                // GET
                case .getPin:
                    self.delegate!.getPinFailure()
                    
                // POST
                case .postPin:
                    self.delegate!.postPinFailure()
                    
                }
        })
    }
}
