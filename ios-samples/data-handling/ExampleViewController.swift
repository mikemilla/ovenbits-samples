//
//  ExampleViewController.swift
//  API Request Template and Usage Examples
//
//  Created by Michael Miller on 11/1/17.
//  Copyright � 2017 mikemiller.design. All rights reserved.
//

import UIKit

// MARK: Example Model
// Normally I would place this in it's own file with other data models
struct Course: Decodable {
    let id: Int
    let name: String
    let link: String
    let imageUrl: String
}

// MARK: Example View Controller and API Request Usage
class ExampleViewController: UIViewController, APIDelegate {

    // MARK: Outlets
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    
    // MARK: View Delegate
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Init api reference
        let api = API()
        
        // If you'd like api requests to call delegate methods set delegate
        api.delegate = self
        
        // Perform request using a delegate
        api.getCourses(completion: nil)
        
        // If you would like to handle requests with a completion handler
        // You can use this
        api.getCourses { (courses) in
            
            // Ensure we have courses
            if let c = courses {
                self.label.text = c[0].name
            } else {
                self.label.text = "No courses found"
            }

            // End loading indication
            self.loadingIndicator.stopAnimating()
            
        }
    }
    
    // MARK: API Delegate
    // If you would like to handle requests with delegate methods
    // You can use these
    func didGetCoursesSuccess(courses: [Course]) {
        label.text = courses[0].name
        loadingIndicator.stopAnimating()
    }

    func didGetCoursesFailure(error: Error) {
        print(error)
    }

}
