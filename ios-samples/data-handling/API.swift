//
//  API.swift
//  API Request Template and Usage Examples
//
//  Created by Michael Miller on 11/1/17.
//  Copyright © 2017 mikemiller.design. All rights reserved.
//

import Foundation

// MARK: Delegate Methods
protocol APIDelegate {
    func didGetCoursesSuccess(courses: [Course])
    func didGetCoursesFailure(error: Error)
}

// MARK: Extensions
// This allows us to have optional delegate methods
extension APIDelegate {
    func didGetCoursesSuccess(courses: [Course]) {}
    func didGetCoursesFailure(error: Error) {}
}

// MARK: API Class
class API {
    
    // MARK: Variables
    var delegate: APIDelegate?
    private var currentTask: URLSessionDataTask?
    
    // MARK: Requests
    func getCourses(completion: ((_ courses: [Course]?) -> ())?) {
        
        // Setup endpoint
        let endpoint = "http://api.letsbuildthatapp.com/jsondecodable/courses" // Just a working example, this would be setup to be more extensible in a production app
        let url = URL(string: endpoint)
        
        // Perform request
        currentTask = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            // Check for error
            if let requestError = error {
                
                // Call optional delegate or completion
                DispatchQueue.main.sync {
                    self.delegate?.didGetCoursesFailure(error: requestError)
                    if let completionhandler = completion {
                        completionhandler(nil)
                    }
                }
                
            } else if let responseData = data {
                do {
                    
                    // Get courses from response
                    let courses = try JSONDecoder().decode([Course].self, from: responseData)
                    
                    // Call optional delegate or completion
                    DispatchQueue.main.sync {
                        self.delegate?.didGetCoursesSuccess(courses: courses)
                        if let completionhandler = completion {
                            completionhandler(courses)
                        }
                    }
                    
                } catch let jsonError {
                    DispatchQueue.main.sync {
                        self.delegate?.didGetCoursesFailure(error: jsonError)
                        if let completionhandler = completion {
                            completionhandler(nil)
                        }
                    }
                }
            }
            
        }
        
        // Performs request
        // This will not be nil so we can force unwrap
        currentTask!.resume()
    }
    
    // MARK: Cancel Outgoing Requests
    // This is a helper for cancelling the current request if it exists
    func cancelCurrentRequest() {
        if let task = currentTask {
            task.cancel()
        } else {
            print("There is no current outgoing request 🙊")
        }
    }
    
}
